# DISKCOMP

Disk comparing utility


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## DISKCOMP.LSM

<table>
<tr><td>title</td><td>DISKCOMP</td></tr>
<tr><td>version</td><td>06jun2003 (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2003-06-06</td></tr>
<tr><td>description</td><td>Disk comparing utility</td></tr>
<tr><td>keywords</td><td>diskcomp, compare, disks</td></tr>
<tr><td>author</td><td>Michal Meller &lt;maceman -at- priv4.onet.pl&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Steve Nickolas &lt;steve -at- dosius.zzn.com&gt; Eric Auer &lt;e.auer -at- jpberlin.de&gt;</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.auersoft.eu/soft/by-others/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Diskcomp</td></tr>
</table>
